package com.example.retrofitinandroid3;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface UserManagerInterface {

    /*
     * @FormUrlEncoded : Point out this method will construct a form submit action.
     *
     * @POST : Point out the form submit will use post method,
     * the form action url is the parameter of @POST annotation.
     *
     * @Field("form_field_name") : Indicate the form filed name,
     * the filed value will be assigned to input parameter userNameValue.
     *
     * Call<ResponseBody> If you do not need any type-specific response
     */
    public static String BASE_URL = "http://10.0.2.2/retrofit/";
    @FormUrlEncoded
    @POST("register.php")
    public Call<ResponseBody> registerUser(@Field("username") String userNameValue,
                                           @Field("password") String passwordValue,
                                           @Field("email") String emailValue);

}
