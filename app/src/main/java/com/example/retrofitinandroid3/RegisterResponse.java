package com.example.retrofitinandroid3;

public class RegisterResponse {
    private boolean success = false;
    private String message = "";
    public boolean isSuccess() {
        return success;
    }
    public void setSuccess(boolean success) {
        this.success = success;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

}
